@foreach($foos as $foo)
    <div>
        Foo: {{ $foo->title }}
        @if($foo->bar()->exists())
            | Bar: {{ $foo->bar->title }}
        @endif
    </div>
@endforeach
