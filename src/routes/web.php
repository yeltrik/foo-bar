<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\FooBar\app\http\controllers\FooController;

Route::get('/foo',
    [FooController::class, 'index'])
    ->name('foos.index');
