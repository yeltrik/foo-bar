<?php

namespace Yeltrik\FooBar\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yeltrik\FooBar\app\models\Foo;

class FooController extends Controller
{

    public function __construct()
    {
        $this->middleware([
           'web',
           'auth'
        ]);
    }

    public function index()
    {
        $this->authorize('viewAny', Foo::class);

        $foos = Foo::query()
            ->paginate(10);

        return view('FooBar::foo.index', compact(
            'foos'
        ));
    }

}
