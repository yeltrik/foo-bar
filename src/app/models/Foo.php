<?php

namespace Yeltrik\FooBar\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Foo extends Model
{
    use HasFactory;

    public function bar()
    {
        return $this->belongsTo(Bar::class);
    }

}
