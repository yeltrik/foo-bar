<?php

namespace Yeltrik\FooBar\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bar extends Model
{
    use HasFactory;

    public function foos()
    {
        return $this->hasMany(Foo::class);
    }

}
