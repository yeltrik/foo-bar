<?php

namespace Yeltrik\FooBar\app\policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FooPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return true;
    }
}
