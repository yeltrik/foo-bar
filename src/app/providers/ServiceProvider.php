<?php

namespace Yeltrik\FooBar\app\providers;

use Illuminate\Support\Arr;
use function PHPUnit\Framework\fileExists;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        if ( file_exists(config_path('foobar-database-connections.php')) ) {
            // User Customizable
            $this->mergeConfigFrom(
                config_path('foobar-database-connections.php'), 'database.connections'
            );
            //dd(config('database'));
        } else {
            // Non Customizable
            $this->mergeConfigFrom(
                __DIR__ . '/../../config/database-connections.php', 'database.connections'
            );
            //dd(config('database'));
        }

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'FooBar');

        if ($this->app->runningInConsole()) {
            $this->publishResources();
        }
    }

    protected function publishResources()
    {
        // User Customizable
        $this->publishes([
            __DIR__ . '/../../config/database-connections.php' => config_path('foobar-database-connections.php'),
        ], 'config');

        $this->publishes([
            __DIR__ . '/../../database/seeders/FooSeeder.php' => database_path('seeders/FooSeeder.php'),
            __DIR__ . '/../../database/seeders/BarSeeder.php' => database_path('seeders/BarSeeder.php'),
        ], 'foobar');
    }

}
